# Python_upgrade



## Getting started
1. Start by updating the repositories:
    sudo apt update

2. Next, install Python 3.9 by running:
    sudo apt install python3.9

3. Once Python installs, invoke the 3.9 version by running:
    python3.9

4. However, checking the installation with the python3 --version command still returns the old version. To fix this, you need to create a list of update alternatives. First, add the old version to the list with the command:
    
    sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.[old-version] 1

5. Now add the new version:
    sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.9 2

6. Next, type the following command to configure the priority status of the versions:
   
    sudo update-alternatives --config python3
  The output displays the available choices and their assigned number (in the example below, the numbers are     0, 1, 2). Type the number of the the version you wish to use and press Enter.

7. If you are not planning to use the old version of Python, remove the symlink that contained the previous Python 3 version with:
    sudo rm /usr/bin/python3

8. Then, replace the symlink with the new version:
    sudo ln -s python3.9 /usr/bin/python3

9. Now, check the default version:
    python3 --version
       The output should confirm the successful installation and setup of the latest available version.



       Refernce: https://phoenixnap.com/kb/upgrade-python

                 https://www.itsupportwale.com/blog/how-to-upgrade-to-python-3-9-0-on-ubuntu-18-04-lts/



                 
